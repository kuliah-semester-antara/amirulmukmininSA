<html>
<?php
require_once '../config/config.php';
?>

<head>
	<title>Native MVC Example</title>
	<link rel="stylesheet" href="/mvc-example/assets/css/bootstrap.css" />
	<script type="text/javascript" src="/mvc-example/assets/js/bootstrap.js"></script>
</head>

<body>
	<div class=" d-flex flex-column justify-content-center flex-wrap mx-4">

		<div class="pt-5 container">
			<h3>Halo! Ini adalah program sederhana dengan arsitektur MVC</h3>
			<h3>Isikan data Anda di sini</h3>
			<form method="post" action="/mvc-example/?act=simpan">
				<div class="form-group">
					<label for="exampleInputNim">NIM</label>
					<input type="text" class="form-control" id="exampleInputNim" name="nim" placeholder="NIM Anda">
				</div>
				<div class="form-group mt-4">
					<label for="exampleInputNama">Nama</label>
					<input type="text" class="form-control" id="exampleInputNama" name="nama" placeholder="Nama (boleh nama lengkap/panggilan)">
				</div>

				<button type="submit" class="btn btn-primary mt-5">Submit</button>
			</form>
			<a href="/mvc-example/?act=tampil-data" class="btn btn-success">Lihat Hasil Input</a>
		</div>
	</div>
	</div>
</body>

</html>